function getCookie(name) {
    var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg))
        return unescape(arr[2]);
    else
        return null;
}

function SetCookie(n, v) {
    var Then = new Date(2050, 1, 1);
    document.cookie = n + "=" + v + ";expires=" + Then.toGMTString() + ";path=/";
}

function setReadHistory() {
    var bookInfoEle = document.getElementById('content');
    var bookinfo = {};
    bookinfo.bid = bookInfoEle.getAttribute('data-bid');
    bookinfo.aid = bookInfoEle.getAttribute('data-aid');
    bookinfo.cid = bookInfoEle.getAttribute('data-cid');
    bookinfo.bookname = bookInfoEle.getAttribute('data-bookname');
    bookinfo.title = bookInfoEle.getAttribute('data-title');
    var _json = getReadHistory();
    if (_json == null) {
        SetCookie("ReadHistory", "[" + getHistoryStr(bookinfo.aid, bookinfo.bookname, bookinfo.bid, bookinfo.title) + "]");
        return;
    }
    var _has = false;
    for (i = 0; i < _json.length; i++) {
        if (_json[i].aid == bookinfo.aid)
            _has = true;
    }
    var _jstr = "[";
    if (_has == true) {
        for (i = 0; i < _json.length; i++) {
            if (_json[i].aid == bookinfo.aid)
                _jstr += getHistoryStr(bookinfo.aid, bookinfo.bookname, bookinfo.bid, bookinfo.title);
            else
                _jstr += getHistoryStr(_json[i].aid, _json[i].name, _json[i].bid, _json[i].title);
            if (i < (_json.length - 1))
                _jstr += ",";
        }
    } else {
        var _start = 0;
        if (_json.length == 1) /*此处控制写入cookie条数*/
            _start = 1;
        for (i = _start; i < _json.length; i++)//把现有的遍历完
        {
            _jstr += getHistoryStr(_json[i].aid, _json[i].name, _json[i].bid, _json[i].title);
            _jstr += ",";
        }
        _jstr += getHistoryStr(bookinfo.aid, bookinfo.bookname, bookinfo.bid, bookinfo.title);
    }//if
    _jstr += "]";
    SetCookie("ReadHistory", _jstr);
}

function getHistoryStr(aid, bookname, bid, title) {
    return "{\"aid\":" + aid + ",\"name\":\"" + escape(bookname) + "\",\"bid\":" + bid + ",\"title\":\"" + escape(title) + "\"}";
}

function getReadHistory() {
    var _history = getCookie("ReadHistory");//getCookiesWithKey("ReadHistory","");
    if (_history == "")
        return null;
    _history = unescape(_history);
    return eval(_history);
}

//init Histories Cookies
setReadHistory();
userLoginStatus();

function userLoginStatus() {
    var isLoginEle = document.getElementsByClassName('member-login')[0];
    if (!isLoginEle) return;
    var xhr = xhrObj();
    xhr.open('GET', '/e/member/login/loginjs.php');
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var data = JSON.parse(xhr.responseText);
            if (data.code == 1) {
                isLoginEle.innerHTML = data.data;
            }
        }
    };
    xhr.send();
}

function showAndHideChapters() {
    var chapterListBtn = document.getElementById('chapter-list');
    if (!chapterListBtn) return;
    var chapterListBlock = document.getElementById('chapter-list-block');
    var bookDetailMain = document.getElementById('book-detail-main');
    chapterListBtn.onclick = function () {
        var nowStyle = chapterListBlock.style.display;
        if (nowStyle == 'none' || nowStyle == '') {
            chapterListBlock.style.display = 'block';
            bookDetailMain.style.display = 'none';
            this.text = '\u6536\u8d77\u7ae0\u8282\u76ee\u5f55';
        } else {
            chapterListBlock.style.display = 'none';
            bookDetailMain.style.display = 'block';
            this.text = '\u5c0f\u8bf4\u7ae0\u8282\u76ee\u5f55';
        }
    }
}

function xhrObj() {
    var xhr;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest;
    } else {
        var ver = ["Microsoft.XMLHTTP", "Msxml2.XMLHTTP", "Msxml2.XMLHTTP.3.0", "Msxml2.XMLHTTP.5.0", "Msxml2.XMLHTTP.6.0"];
        for (var i in ver) {
            try {
                xhr = new ActiveXObject(ver[i]);
            } catch (e) {
            }
        }
    }
    return xhr;
}