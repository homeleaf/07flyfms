!function (t, e) {
    "function" == typeof define && define.amd ? define(function () {
        return e(t)
    }) : "object" == typeof exports ? module.exports = e : t.echo = e(t)
}(this, function (t) {
    "use strict";
    var e, n, o, r, c, a = {}, u = function () {
    }, d = function (t) {
        return null === t.offsetParent
    }, l = function (t, e) {
        if (d(t)) return !1;
        var n = t.getBoundingClientRect();
        return n.right >= e.l && n.bottom >= e.t && n.left <= e.r && n.top <= e.b
    }, i = function () {
        (r || !n) && (clearTimeout(n), n = setTimeout(function () {
            a.render(), n = null
        }, o))
    };
    return a.init = function (n) {
        n = n || {};
        var d = n.offset || 0, l = n.offsetVertical || d, f = n.offsetHorizontal || d, s = function (t, e) {
            return parseInt(t || e, 10)
        };
        e = {
            t: s(n.offsetTop, l),
            b: s(n.offsetBottom, l),
            l: s(n.offsetLeft, f),
            r: s(n.offsetRight, f)
        }, o = s(n.throttle, 250), r = n.debounce !== !1, c = !!n.unload, u = n.callback || u, a.render(), document.addEventListener ? (t.addEventListener("scroll", i, !1), t.addEventListener("load", i, !1)) : (t.attachEvent("onscroll", i), t.attachEvent("onload", i))
    }, a.render = function (n) {
        for (var o, r, d = (n || document).querySelectorAll("[data-echo], [data-echo-background]"), i = d.length, f = {
            l: 0 - e.l,
            t: 0 - e.t,
            b: (t.innerHeight || document.documentElement.clientHeight) + e.b,
            r: (t.innerWidth || document.documentElement.clientWidth) + e.r
        }, s = 0; i > s; s++) r = d[s], l(r, f) ? (c && r.setAttribute("data-echo-placeholder", r.src), null !== r.getAttribute("data-echo-background") ? r.style.backgroundImage = "url(" + r.getAttribute("data-echo-background") + ")" : r.src !== (o = r.getAttribute("data-echo")) && (r.src = o), c || (r.removeAttribute("data-echo"), r.removeAttribute("data-echo-background")), u(r, "load")) : c && (o = r.getAttribute("data-echo-placeholder")) && (null !== r.getAttribute("data-echo-background") ? r.style.backgroundImage = "url(" + o + ")" : r.src = o, r.removeAttribute("data-echo-placeholder"), u(r, "unload"));
        i || a.detach()
    }, a.detach = function () {
        document.removeEventListener ? t.removeEventListener("scroll", i) : t.detachEvent("onscroll", i), clearTimeout(n)
    }, a
});

window.onload = function () {
    echo.init({offset: 100, throttle: 100, unload: false});
    searchCheck();
    showAndHideChapters();
    setLastRead();
    getFileUrl();
    userLoginStatus();
};

function userLoginStatus() {
    var isLoginEle = document.getElementsByClassName('member-login')[0];
    if (!isLoginEle) return;
    var xhr = xhrObj();
    xhr.open('GET', '/e/member/login/loginjs.php');
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var data = JSON.parse(xhr.responseText);
            if (data.code == 1) {
                isLoginEle.innerHTML = data.data;
            }
        }
    };
    xhr.send();
}

function showAndHideChapters() {
    var chapterListBtn = document.getElementById('chapter-list');
    if (!chapterListBtn) return;
    var chapterListBlock = document.getElementById('chapter-list-block');
    var bookDetailMain = document.getElementById('book-detail-main');
    chapterListBtn.onclick = function () {
        var nowStyle = chapterListBlock.style.display;
        if (nowStyle == 'none' || nowStyle == '') {
            chapterListBlock.style.display = 'block';
            bookDetailMain.style.display = 'none';
            this.text = '\u6536\u8d77\u7ae0\u8282\u76ee\u5f55';
        } else {
            chapterListBlock.style.display = 'none';
            bookDetailMain.style.display = 'block';
            this.text = '\u5c0f\u8bf4\u7ae0\u8282\u76ee\u5f55';
        }
    }
}

function xhrObj() {
    var xhr;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest;
    } else {
        var ver = ["Microsoft.XMLHTTP", "Msxml2.XMLHTTP", "Msxml2.XMLHTTP.3.0", "Msxml2.XMLHTTP.5.0", "Msxml2.XMLHTTP.6.0"];
        for (var i in ver) {
            try {
                xhr = new ActiveXObject(ver[i]);
            } catch (e) {
            }
        }
    }
    return xhr;
}

function searchCheck() {
    var addform = document.getElementById('searchform');
    if (!addform) return;
    addform.onsubmit = function () {
        if (addform.keyboard.value.length < 2) {
            alert("\u8bf7\u8f93\u5165\u4e66\u540d\u6216\u4f5c\u8005\u540e\u5c1d\u8bd5\u641c\u7d22");
            addform.keyboard.focus();
            return false;
        }
    }
}

function menuSwitch() {
    var navObj = document.getElementById('nav-switch');
    var ncss = navObj.style.display;
    console.log(ncss);
    if (ncss != 'block') {
        navObj.style.display = 'block';
    } else {
        navObj.style.display = 'none';
    }
}

function getFileUrl() {
    var fileEle = document.getElementById('download-txt');
    if (!fileEle) return;
    var bookInfosEle = document.getElementById('book-infos');
    //var cid = bookInfosEle.getAttribute('data-cid');
    var bid = bookInfosEle.getAttribute('data-bid');
    fileEle.onclick = function () {
        window.open('/down/' + bid + '.html', '_self');
    }
}

function setLastRead() {
    var bookinfo = {};
    var bookInfosEle = document.getElementById('book-infos');
    if (!bookInfosEle) return;
    bookinfo.cid = bookInfosEle.getAttribute('data-cid');
    bookinfo.bid = bookInfosEle.getAttribute('data-bid');
    if (bookinfo == false) return;
    var _history = getCookiesWithKey("ReadHistory", "");
    if (_history == "") return;
    var _json = eval(unescape(_history));
    if (_json == null) return;
    for (i = 0; i < _json.length; i++) {
        if (_json[i].bid == bookinfo.bid) {
            if (_json[i].bid == "") return;
            var url = "/read/" + bookinfo.cid + "/" + bookinfo.bid + "/" + _json[i].aid + ".html";
            var lastReadEle = document.getElementById('last-read');
            var continueReading = document.getElementById('continue-reading');
            lastReadEle.style.display = 'block';
            continueReading.innerHTML = '\u7ee7\u7eed\u9605\u8bfb\u672a\u8bfb\u7ae0\u8282';
            continueReading.setAttribute('href', url);
            lastReadEle.innerHTML = '<div class="title">\u60a8\u5df2\u9605\u8bfb ' + _json[i].name + ' \u81f3 <a href="' + url + '">' + _json[i].title + '</a></div>';
            return;
        }
    }
}

function getCookiesWithKey(key, c_name) {
    if (document.cookie.length > 0) {
        var k_start = document.cookie.indexOf(key + "=");
        if (k_start == -1)
            return "";
        k_start = k_start + key.length + 1;
        var k_end = document.cookie.indexOf(";", k_start);
        if (k_end == -1) k_end = document.cookie.length;
        var cookiesWithKey = unescape(document.cookie.substring(k_start, k_end));
        if (c_name == "") return cookiesWithKey;
        var cookies = cookiesWithKey.split("&");
        for (var i = 0; i < cookies.length; i++) {
            if (cookies[i].split("=")[0] == c_name) {
                return cookies[i].split("=")[1];
            }
        }
    }
    return '';
}