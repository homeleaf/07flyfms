function setContent(intype) {
    var huyandiv = document.getElementById("huyan"),
        light = document.getElementById("light").innerText;
    if (intype == "huyan") {
        if (huyandiv.style.backgroundColor == "") {
            set("light", "huyan");
            SetCookie('light', 'huyan', '1');
        } else {
            set("light", "no");
            SetCookie('light', 'no', '1');
        }
    }
    if (intype == "light") {
        if (light == "关灯") {
            set("light", "yes");
            SetCookie('light', 'yes', '1');
        } else {
            set("light", "no");
            SetCookie('light', 'no', '1');
        }
    }
    if (intype == "big") {
        set("font", "big");
        SetCookie('font', 'big', '30');
    }
    if (intype == "middle") {
        set("font", "middle");
        SetCookie('font', 'middle', '1');
    }
    if (intype == "small") {
        set("font", "small");
        SetCookie('font', 'small', '30');
    }
}

function set (intype, p) {
    if (intype == "light") {
        var light = document.getElementById('light'),
            huyan = document.getElementById('huyan'),
            readBlock = document.getElementById('read-block');
        if (p == "yes") {
            light.innerText = '开灯';
            readBlock.className = 'read-content-block light';
            light.style.backgroundColor = '#999';
            light.style.color = '#000';
            huyan.style.backgroundColor = '';
            huyan.style.color = '';
        } else if (p == "no") {
            light.innerText = '关灯';
            readBlock.className = 'read-content-block';
            light.style.backgroundColor = '';
            light.style.color = '';
            huyan.style.backgroundColor = '';
            huyan.style.color = '';
        } else if (p == "huyan") {
            light.innerText = '关灯';
            readBlock.className = 'read-content-block huyan';
            light.style.backgroundColor = '';
            light.style.color = '';
            huyan.style.backgroundColor = '#9bc7a0';
            huyan.style.color = '#00774e';
        }
    }

    if (intype == "font") {
        var content = document.getElementById('content');
        if (p == "big") {
            content.style.fontSize = '24px';
        }
        if (p == "middle") {
            content.style.fontSize = '20px';
        }
        if (p == "small") {
            content.style.fontSize = '';
        }
    }
}

function getset() {
    var light = getCookie('light', '', '; '),
        font = getCookie('font', '', '; ');
    if (light == "yes") {
        set("light", "yes");
    } else if (light == "no") {
        set("light", "no");
    } else if (light == "huyan") {
        set("light", "huyan");
    }
    if (font == "big") {
        set("font", "big");
    } else if (font == "middle") {
        set("font", "middle");
    } else if (font == "small") {
        set("font", "small");
    }
}

function getCookie(Name, Cookies, fei) {
    var search = Name + "=", returnvalue = "";
    if (!Cookies) {
        Cookies = document.cookie
    }
    if (Cookies.length > 0) {
        offset = Cookies.indexOf(search);
        if (offset != -1) {
            offset += search.length;
            end = Cookies.indexOf(fei, offset);
            if (end == -1)
                end = Cookies.length;
            returnvalue = Cookies.substring(offset, end);
        }
    }
    return returnvalue;
}

function SetCookie(name, value, day) {
    if (!day) {
        day = 30;
    }
    var Then = new Date();
    if (day == 1) {
        var curTamp = Then.getTime();
        var curWeeHours = new Date(Then.toLocaleDateString()).getTime() - 1;
        var passedTamp = curTamp - curWeeHours;
        var leftTamp = 24 * 60 * 60 * 1000 - passedTamp;
        var leftTime = new Date();
        leftTime.setTime(leftTamp + curTamp);
        document.cookie = name + "=" + escape(value) + ";expires=" + leftTime.toGMTString() + ";path=/;";
    } else {
        Then.setTime(Then.getTime() + day * 24 * 60 * 60 * 1000);
        document.cookie = name + "=" + escape(value) + ";expires=" + Then.toGMTString() + ";path=/;";
    }
}

getset();
