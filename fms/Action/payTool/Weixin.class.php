<?php 
/**
 * The Temp file of AAA.
 *
 * All Manager Finance , orderinfo
 *
 * @copyright   Copyright (C) 2012-2015 07FLY Network Technology Co,LTD (www.07FLY.com)
 *				All rights reserved.
 * @license     LGPL 
 * @author      NIAOMUNIAO <1585925559@QQ.com>
 * @package     system
 * @version     1.0
 * @link        http://www.07fly.net
 * @version   Weixin.class.php  add by NIAOMUNIAO 2015-07-27 16:48 
 */	 

/**
 * Enter description here... （必须）
 *
 * [example]
 * $Ajax = new Weixin ();
 * $Ajax->main();
 * $Ajax = $user->ajax_package_option($uid);  * 
 * [/example]
 * 
 */

class Weixin extends Action{	
	private $cacheDir='';//缓存目录
	
	//系统常规设置
	public function weixin_config(){
		if(empty($_POST)){
			$config = $this->get_weixin_info();
			$smarty = $this->setSmarty();
			$smarty->assign(array("one"=>$config));//框架变量注入同样适用于smarty的assign方法
			$smarty->display('weixin/weixin_config.html');					
		}else{
			foreach($_POST as $key=>$v){
				$sql="INSERT INTO fly_config_weixin(name,value) VALUES('$key','$v') 
						ON DUPLICATE KEY UPDATE value='$v'";
				$this->C($this->cacheDir)->update($sql);
			}
			$this->L("Common")->ajax_json_success("操作成功");
		}
	}	
	//得到配置参数
	public function get_weixin_info(){
		$sql 	= "select * from fly_config_weixin;";
		$list	= $this->C($this->cacheDir)->findAll($sql);
		if(is_array($list)){
			foreach($list as $key=>$row){
				$assArr[$row["name"]] = $row["value"];
			}
		}
		return $assArr;		
	}	
	//发起微信支付请求动作
	public function weixin_action($data){
		$weixin 		=$this->get_weixin_info();
		$WXout_trade_no	=$data["WIDout_trade_no"];
		$WXsubject		=$data["WIDsubject"];
		$WXtotal_fee	=$data["WIDtotal_fee"];
		$WXbody			=$data["WIDbody"];

/*		echo "订单编号：".$this->_REQUEST("WXout_trade_no")."<br>";
		echo "商品名称：".$this->_REQUEST("WXsubject")."<br>";
		echo "付款金额：".$this->_REQUEST("WXtotal_fee")."<br>";
		echo "订单描述：".$this->_REQUEST("WXbody")."<br>";
*/
		
		$url	  ="{$weixin['weixin_domain']}/weixin/api.php";
		$post_data=array(
						'account'=>$_SESSION['front_user'],
						'weixin_appid'=>$weixin['weixin_appid'],
						'weixin_appsecret'=>$weixin['weixin_appsecret'],
						'weixin_mchid'=>$weixin['weixin_mchid'],
						'weixin_key'=>$weixin['weixin_key'],
						'weixin_domain'=>$weixin['weixin_domain'],
						'weixin_notify'=>$weixin['weixin_notify'],
						'WXout_trade_no'=>$WXout_trade_no,
						'WXsubject'=>$WXsubject,
						'WXtotal_fee'=>$WXtotal_fee,
						'WXbody'=>$WXbody,
					);
		$rtn=$this->L("Common")->open_curl($url,$post_data);
/*		echo "<hr>返回信息：<br>";
		echo $rtn;
		echo "<br>返回信息结束<hr>";		
		exit;*/

		$url	 ="{$weixin['weixin_domain']}/weixin/default.php?id=$WXout_trade_no";
		Header("Location: $url"); 

	}
	
	
}//end class
?> 