<?php 
/**
 * The Temp file of AAA.
 *
 * All Manager Finance , orderinfo
 *
 * @copyright   Copyright (C) 2012-2015 07FLY Network Technology Co,LTD (www.07FLY.com)
 *				All rights reserved.
 * @license     LGPL 
 * @author      NIAOMUNIAO <1585925559@QQ.com>
 * @package     system
 * @version     1.0
 * @link        http://www.07fly.net
 * @version   Alipay.class.php  add by NIAOMUNIAO 2015-07-27 16:48 
 */	 

/**
 * Enter description here... （必须）
 *
 * [example]
 * $Ajax = new Alipay ();
 * $Ajax->main();
 * $Ajax = $user->ajax_package_option($uid);  * 
 * [/example]
 * 
 */

class Alipay extends Action{	
	private $cacheDir='';//缓存目录
	
	//系统常规设置
	public function alipay_config(){
		if(empty($_POST)){
			$config = $this->get_alipay_info();
			$smarty = $this->setSmarty();
			$smarty->assign(array("one"=>$config));//框架变量注入同样适用于smarty的assign方法
			$smarty->display('alipay/alipay_config.html');					
		}else{
			foreach($_POST as $key=>$v){
				$sql="INSERT INTO fly_config_alipay(name,value) VALUES('$key','$v') 
						ON DUPLICATE KEY UPDATE value='$v'";
				$this->C($this->cacheDir)->update($sql);
			}
			$this->L("Common")->ajax_json_success("操作成功");
		}
	}	
	//得到配置参数
	public function get_alipay_info(){
		$sql 	= "select * from fly_config_alipay;";
		$list	= $this->C($this->cacheDir)->findAll($sql);
		if(is_array($list)){
			foreach($list as $key=>$row){
				$assArr[$row["name"]] = $row["value"];
			}
		}
		return $assArr;		
	}	
	public function alipay_action($data){
		$config=$this->get_alipay_info();
		$account		=$data["account"];
		$WXout_trade_no	=$data["WIDout_trade_no"];
		$WXsubject		=$data["WIDsubject"];
		$WXtotal_fee	=$data["WIDtotal_fee"];
		$WXbody			=$data["WIDbody"];
		
		$partner      		= $config["alipay_id"];
		$key          		= $config["alipay_key"];
		$seller_email 		= $config["alipay_email"];
		$alipay_return_url  = $config["alipay_return_url"];
		
		$return_url   = $alipay_return_url ."/Aliplay/return_url.php";
		$notify_url   = $alipay_return_url ."/Aliplay/notify_url.php";
		$cacert		  = "/Aliplay/cacert.pem";	
		
		$url	  ='http://'.$_SERVER['HTTP_HOST']."/Alipay/api.php";
		$post_data=array(
			'account'=>$account,
			'WXout_trade_no'=>$WXout_trade_no,
			'WXsubject'=>$WXsubject,
			'WXtotal_fee'=>$WXtotal_fee,
			'WXbody'=>$WXbody,			
			
			'alipay_return_url'=>$alipay_return_url,//返回域名
			'notify_url' 	=> $notify_url,
			'return_url' 	=> $return_url,
			'cacert' 		=> $cacert,
			'partner'      	=> $partner,
			'key'          	=> $key,
			'seller_email' => $seller_email,
		);	
		echo "alipay_action <hr>";
		$rtn=$this->L("Common")->open_curl($url,$post_data);
		$url	 ="{$config['alipay_return_url']}/Alipay/alipayapi.php?id=$WXout_trade_no";
		Header("Location: $url"); 

	}
	
	
}//end class
?> 