<?php
/*
 * 小说管理类
 *
 * @copyright   Copyright (C) 2017-2018 07FLY Network Technology Co,LTD (www.07FLY.com) All rights reserved.
 * @license     For licensing, see LICENSE.html or http://www.07fly.top/fms/license
 * @author      kfrs <goodkfrs@QQ.com>
 * @package     home.Book
 * @version     1.0
 * @link       http://www.07fly.top
 */	 
class Book extends Action {

	private $cacheDir = 'c_home'; //缓存目录
	private $smarty = '';
	private $assign =array();
	public function __construct() {
		require(ACTION . 'lib/conn.php');
		$this->smarty =$this->setSmarty();
		$this->assign['global']=$this->L('home/Config')->get_sys_config();
		$this->smarty->register_block("typelist","typelist");
		$this->smarty->register_block("booklist","booklist");
		$this->smarty->register_block("chaplist","chaplist");
		
	}
	
	public function book_show(){
		$bookid =$this->_REQUEST("bid");
		$one	=$this->book_get_one();
		$this->smarty->bookid=$bookid;//赋值
		$this->assign['field']=$one;
		$this->smarty->assign(array('fly'=>$this->assign));
		$this->smarty->display('home/book_show.html');
	}
	
	public function book_down(){
		$bookid =$this->_REQUEST("bid");
		$one	=$this->book_get_one();
		$this->smarty->bookid=$bookid;//赋值
		$this->assign['field']=$one;
		$this->smarty->assign(array('fly'=>$this->assign));
		$this->smarty->display('home/book_down.html');
	}

	public function book_chap(){
		$bookid =$this->_REQUEST("bid");
		$one	=$this->book_get_one();
		$this->smarty->bookid=$bookid;//赋值
		$this->assign['field']=$one;
		$this->smarty->assign(array('fly'=>$this->assign));
		$this->smarty->display('home/book_chap.html');
	}
	
	
	//搜索
	public function book_search(){
		$searchkey =$this->_REQUEST("searchkey");
		$searchval =$this->_REQUEST("searchval");
		$this->smarty->searchkey=$searchkey;//赋值
		$this->smarty->searchval=$searchval;//赋值
		$this->assign['field']=array("searchkey"=>$searchkey,"searchval"=>$searchval);
		$this->smarty->assign(array('fly'=>$this->assign));	
		$this->smarty->display('home/book_search.html');
	}
	
	public function book_get_one(){
		$bookid=$this->_REQUEST("bid");
		$sql="select b.id,b.name,b.typeid,b.overs,b.adt,b.udt,b.writer,b.booksize,b.img,b.intro,t.typename 
					from fly_book as b,fly_book_type as t 
					where b.typeid=t.id and b.id='$bookid'";
		$one=$this->C($this->cacheDir)->findOne($sql);	
		//$one['downurl'] =ACT."/home/Book/book_down/bid/".$one['id']."/";
		//$one['bookurl'] =ACT."/home/Book/book_show/bid/".$one['id']."/";
		//$one['bookchap']=ACT."/home/Book/book_chap/bid/".$one['id']."/";
		$one['typeurl'] =urlswitch('typeurl',$one['typeid']);
		$one['downurl'] =urlswitch('downurl',$one['id']);
		$one['bookurl'] =urlswitch('bookurl',$one['id']);
		$one['bookchap']=urlswitch('bookchap',$one['id']);
		$one['bookimg'] =APP."/Cache".$one['img'];
		$one['overs']  =($one['overs']==1)?"已完本":"连载中";
		return $one;
	}
	
	

} //

?>