<?php
/*
 * 配置管理类
 *
 * @copyright   Copyright (C) 2017-2018 07FLY Network Technology Co,LTD (www.07FLY.com) All rights reserved.
 * @license     For licensing, see LICENSE.html or http://www.07fly.top/fms/license
 * @author      kfrs <goodkfrs@QQ.com>
 * @package     home.Config
 * @version     1.0
 * @link       http://www.07fly.top
 */	 
 
class Config extends Action{	
	
	private $cacheDir='';//缓存目录
	//得到系统配置参数
	public function get_sys_config(){
		$sql 	= "select * from fly_sys_config;";
		$list	= $this->C($this->cacheDir)->findAll($sql);
		$assArr = array();
		if(is_array($list)){
			foreach($list as $key=>$row){
				$assArr[$row["varname"]] = $row["value"];
			}
		}
		return $assArr;		
	}
	
}//end class
?>