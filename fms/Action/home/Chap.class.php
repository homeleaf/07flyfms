<?php
/*
 * 小说章节管理类
 *
 * @copyright   Copyright (C) 2017-2018 07FLY Network Technology Co,LTD (www.07FLY.com) All rights reserved.
 * @license     For licensing, see LICENSE.html or http://www.07fly.top/fms/license
 * @author      kfrs <goodkfrs@QQ.com>
 * @package     home.Chap
 * @version     1.0
 * @link       http://www.07fly.top
 */	 
class Chap extends Action {
	private $cacheDir = 'c_home'; //缓存目录
	private $assign =array();
	public function __construct() {
		require(ACTION . 'lib/conn.php');
		$this->smarty =$this->setSmarty();
		$this->assign['global']=$this->L('home/Config')->get_sys_config();
		$this->smarty->register_block("typelist","typelist");
		$this->smarty->register_block("booklist","booklist");
		$this->smarty->register_block("chaplist","chaplist");
	}
	
	public function chap_show(){
		$chapid=$this->_REQUEST("cid");
		$sql="select c.*,b.name as bookname
					from fly_book as b,fly_book_chap as c 
					where c.bookid=b.id and c.id='$chapid'";
		$one=$this->C($this->cacheDir)->findOne($sql);
		if($one['content']==1){
			$dirname=CACHE."storage".S.$one["typeid"].S.$one["bookid"];
			$chaptxt=$dirname.S.$one["id"].'.txt';
			$one['content']=file_get_contents($chaptxt);
		}
		//更新点击
		$this->chap_click();
		
		//上一页，下一页
		$next=$this->chap_next($chapid,$one['bookid']);
		$prev=$this->chap_prev($chapid,$one['bookid']);
		$one['nexturl']	=$next['chapurl'];
		$one['nextname']=$next['name'];
		$one['prevurl']	=$prev['chapurl'];
		$one['prevname']=$prev['name'];
		$one['typeurl']	=urlswitch('typeurl',$one['typeid']);
		$one['bookurl']	=urlswitch('bookurl',$one['bookid']);
		$one['bookchap']=urlswitch('bookchap',$one['bookid']);
		$one['downurl']	=urlswitch('downurl',$one['bookid']);
		$this->smarty->bookid=$one["bookid"];
		$this->assign['field']=$one;
		$this->smarty->assign(array('fly'=>$this->assign));
		$this->smarty->display('home/chap_show.html');
	}
	
	//下一条
	public function chap_next($cid,$bid){
		$sql="select c.name,c.id
					from fly_book as b,fly_book_chap as c 
					where c.bookid=b.id and c.bookid='$bid' and c.id>'$cid' order by c.id asc limit 1";
		$one=$this->C($this->cacheDir)->findOne($sql);
		if(empty($one)){
			$one['chapurl']='#';	
			$one['name']='没有了';	
		}else{
			//$one['chapurl']=ACT."/home/Chap/chap_show/cid/".$one['id']."/";
			$one['chapurl']=urlswitch('chapurl',$one['id']);
		}
		return $one;
	}
	
	//上一条
	public function chap_prev($cid,$bid){
		$sql="select c.name,c.id
					from fly_book as b,fly_book_chap as c 
					where c.bookid=b.id and c.bookid='$bid' and c.id<'$cid' order by c.id desc  limit 1";
		$one=$this->C($this->cacheDir)->findOne($sql);
		if(empty($one)){
			$one['chapurl']='#';	
			$one['name']='没有了';	
		}else{
			//$one['chapurl']=ACT."/home/Chap/chap_show/cid/".$one['id']."/";
			$one['chapurl']=urlswitch('chapurl',$one['id']);
		}
		return $one;
	}
	
	public function chap_click(){
		$chapid=$this->_REQUEST("cid");
		$sql="update fly_book_chap set click=click+1 where id='$chapid'";
		$this->C($this->cacheDir)->updt($sql);
	}

} //

?>