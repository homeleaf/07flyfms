<?php
/*
 * 小说分类管理类
 *
 * @copyright   Copyright (C) 2017-2018 07FLY Network Technology Co,LTD (www.07FLY.com) All rights reserved.
 * @license     For licensing, see LICENSE.html or http://www.07fly.top/fms/license
 * @author      kfrs <goodkfrs@QQ.com>
 * @package     home.BookType
 * @version     1.0
 * @link       http://www.07fly.top
 */	 
class BookType extends Action {

	private $cacheDir = 'c_home'; //缓存目录
	private $assign =array();
	
	public function __construct() {
		require(ACTION . 'lib/conn.php');
		$this->assign['global']=$this->L('home/Config')->get_sys_config();
		$this->smarty =$this->setSmarty();
		$this->smarty->register_block("typelist","typelist");
		$this->smarty->register_block("booklist","booklist");
		$this->smarty->register_block("chaplist","chaplist");
	}
	
	public function typelist(){
		$typeid =$this->_REQUEST('tid');
		$pagenum=$this->_REQUEST('pagenum');
		$one	=$this->type_get_one();
		$this->smarty->typeid=$typeid;//赋值
		$this->smarty->pagenum=$pagenum;//赋值
		$this->assign['field']=$one;
		$this->smarty->assign(array('fly'=>$this->assign));
		$this->smarty->display('home/type_list.html');	
	}
	
	//全本列表
	public function shuku(){
		$pagenum=$this->_REQUEST('pagenum');
		$action =$this->_REQUEST('action');
		$this->smarty->pagenum=$pagenum;//赋值
		$this->smarty->action=$action;//赋值查询类型
		$this->smarty->assign(array('fly'=>$this->assign));
		if($action=='rank'){
			$this->smarty->display('home/rank.html');	
		}elseif($action=='overs'){
			$this->smarty->display('home/overs.html');	
		}
		
	}
	public function type_get_one(){
		$tid = $this->_REQUEST("tid");
		$sql = "select * from fly_book_type where id='$tid'";	
		$one = $this->C($this->cacheDir)->findOne($sql);	
		return $one;
	}
	
	
} //

?>