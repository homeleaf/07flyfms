<?php
class Index extends Action {
	private $cacheDir = 'home_c'; //缓存目录
	private $smarty = '';
	private $assign =array();
	
	public function __construct() {
		require(ACTION . 'lib/conn.php');
		$config=$this->L('home/Config')->get_sys_config();
		$this->assign['global']=$config;
		
		//注册一下,smarty 块的方式调用函数.  
		$this->smarty =$this->setSmarty();
		$this->smarty->register_block("typelist","typelist");
		$this->smarty->register_block("booklist","booklist");
		$this->smarty->register_block("chaplist","chaplist");
	}
	public function main(){
		$this->smarty->assign(array('fly'=>$this->assign));
		$this->smarty->display('home/index.html');	
	}


} //
?>