<?php
/*
 * 小说地图管理类
 *
 * @copyright   Copyright (C) 2017-2018 07FLY Network Technology Co,LTD (www.07FLY.com) All rights reserved.
 * @license     For licensing, see LICENSE.html or http://www.07fly.top/fms/license
 * @author      kfrs <goodkfrs@QQ.com>
 * @package     home.Chap
 * @version     1.0
 * @link       http://www.07fly.top
 */	 
class SiteMap extends Action {
	private $cacheDir = 'c_home'; //缓存目录
	private $assign =array();
	public function __construct() {
		require(ACTION . 'lib/conn.php');
		$this->smarty =$this->setSmarty();
		$this->assign['global']=$this->L('home/Config')->get_sys_config();
		$this->smarty->register_block("typelist","typelist");
		$this->smarty->register_block("booklist","booklist");
		$this->smarty->register_block("chaplist","chaplist");
	}
	
	public function sitemapxml(){
		$chapsql	="select id,name from fly_book_chap order by id desc limit 0,10000";
		$chaplist	=$this->C($this->cacheDir)->findAll($chapsql);
		
		$booksql	="select id,name from fly_book order by id desc limit 0,10000";
		$booklist	=$this->C($this->cacheDir)->findAll($booksql);
		
		$this->assign['chaplist']=$chaplist;
		$this->assign['booklist']=$booklist;
		$this->smarty->assign(array('fly'=>$this->assign));
		//$this->smarty->display('home/sitemap.html');
		$output = $this->smarty->fetch('home/sitemap.html');  
		file_put_contents(APP_ROOT . '/sitemap.xml', $output);
	}
	
	

} //

?>