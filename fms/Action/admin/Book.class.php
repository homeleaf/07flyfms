<?php 
/*
 * 小说管理类
 *
 * @copyright   Copyright (C) 2017-2018 07FLY Network Technology Co,LTD (www.07FLY.com) All rights reserved.
 * @license     For licensing, see LICENSE.html or http://www.07fly.top/fms/license
 * @author      kfrs <goodkfrs@QQ.com>
 * @package     admin.Book
 * @version     1.0
 * @link       http://www.07fly.top
 */	 
class Book extends Action{	
	private $cacheDir='c_admin';//缓存目录
	public function __construct() {
		_instance('Action/sysmanage/Auth');
	}	

	public function book($cusID=0){
	
		//**获得传送来的数据作分页处理
		$currentPage= $this->_REQUEST("pageNum");//第几页
		$numPerPage = $this->_REQUEST("numPerPage");//每页多少条
		$currentPage= empty($currentPage)?1:$currentPage;
		$numPerPage = empty($numPerPage)?$GLOBALS["pageSize"]:$numPerPage;
		
		//**************************************************************************
		//**获得传送来的数据做条件来查询
		$searchKeyword	   = $this->_REQUEST("searchKeyword");
		$searchValue	   = $this->_REQUEST("searchValue");
		$name 			   = $this->_REQUEST("name");
		$writer			   = $this->_REQUEST("writer");

		$where_str = " t.id=b.typeid ";
		if( !empty($searchValue) ){
			$where_str .=" and b.$searchKeyword like '%$searchValue%'";
		}
		if(!empty($name)){
			$where_str .=" and b.name like '%$name%'";
		}
		if(!empty($writer)){
			$where_str .=" and b.writer like '%$writer%'";
		}
		
		//**************************************************************************
		$countSql	= "select b.id from fly_book as b,fly_book_type as t where $where_str";
		$totalCount = $this->C($this->cacheDir)->countRecords($countSql);
		$beginRecord= ($currentPage-1)*$numPerPage;
		$sql		= "select b.id,b.name,b.typeid,b.overs,b.adt,b.udt,b.lastchapid,t.typename from fly_book as b,fly_book_type as t 
						where $where_str 
						order by b.udt desc limit $beginRecord,$numPerPage";
		$list		= $this->C($this->cacheDir)->findAll($sql);
		$chap =$this->L('admin/BookChap');
		$over = $this->L('Data')->data_arr('overs');
		foreach($list as $key=>$row){
			$list[$key]['lastchap'] = $chap->book_chap_last_one($row['lastchapid']);
			$list[$key]['adt'] = date("Y-m-d H:i:s",$row['adt']);
			$list[$key]['udt'] = date("Y-m-d H:i:s",$row['udt']);	
		}		
		$assignArray = array('list'=>$list,'over'=>$over,
							 "searchKeyword"=>$searchKeyword,"searchValue"=>$searchValue,
							 "numPerPage"=>$numPerPage,"totalCount"=>$totalCount,"currentPage"=>$currentPage);	
		return $assignArray;
		
	}
	
	//模板输出
	public function book_show(){
			$assArr   = $this->book();
			$smarty   = $this->setSmarty();
			$smarty->assign($assArr);
			$smarty->display('admin/book_show.html');	
	}
	
	//模板输出
	public function book_show_search(){
			$assArr   = $this->book();
			$smarty   = $this->setSmarty();
			$smarty->assign($assArr);
			$smarty->display('admin/book_show_search.html');	
	}	
	//封面采集增加
	public function book_add(){
		if(empty($_POST)){
			$booktype  =$this->L('admin/BookType')->book_type_get_opt('booktype',$value=null);
			$smarty = $this->setSmarty();
			$smarty->assign(array("booktype"=>$booktype));
			$smarty->display('admin/book_add.html');	
		}else{
			
			$name  = $this->_REQUEST("name");
			$writer = $this->_REQUEST("writer");
			$typeid = $this->_REQUEST("type_id");
			$img	= $this->_REQUEST("bookimg_spath");
			$overs 	= $this->_REQUEST("overs");
			$audit 	= $this->_REQUEST("audit");
			$intro 	= $this->_REQUEST("intro");
			/*$click_a= $this->_REQUEST("click_a");
			$click_m= $this->_REQUEST("click_m");
			$click_w= $this->_REQUEST("click_w");
			$recom_a= $this->_REQUEST("recom_a");
			$recom_m= $this->_REQUEST("recom_m");
			$recom_w= $this->_REQUEST("recom_w");*/
			
			$sql   ="insert into fly_book(typeid,name,intro,img,writer,adt) 
							values('$typeid','$name','$intro','$img','$writer','".time()."');";
			$rtn	=$this->C($this->cacheDir)->update($sql);
			//echo $rtn;
			if($rtn>0){
				$this->L("Common")->ajax_json_success("操作成功",'2',"book_add");
			}else{
				$this->L("Common")->ajax_json_error("操作失败");
			}
		}
	}		
	
	//检查书名是否存在
	public function book_exitis_name($name){
		$sql="select name from fly_book where name='$name' ";
		$rtn=$this->C($this->cacheDir)->findOne($sql);
		if(!empty($rtn)) {
			return true;
		}else{
			return false;
		}
	}
	
	//增加小说返回增加的ID号
	public function book_add_save($data){
		$name  = addslashes($data["name"]);
		$intro	= addslashes($data["intro"]);
		$img	= addslashes($data["img"]);
		$writer	= addslashes($data["writer"]);
		$overs	= addslashes($data["overs"]);
		$typeid = $data["typeid"];
		$sql   = "insert into fly_book(typeid,name,intro,img,writer,overs,adt) 
							values('$typeid','$name','$intro','$img','$writer','$overs','".time()."');";
		$rtn	=$this->C($this->cacheDir)->update($sql);
		if($rtn>0){
			return $rtn;
		}else{
			return 0;	
		}
	}
	
	//修改
	public function book_modify(){
		$id	= $this->_REQUEST("id");
		if(empty($_POST)){
			$one 		= $this->book_get_one($id);
			$smarty  	= $this->setSmarty();
			$smarty->assign(array("one"=>$one));
			$smarty->display('admin/book_modify.html');	
		}else{//更新保存数据
			$name  = $this->_REQUEST("name");
			$writer = $this->_REQUEST("writer");
			$typeid = $this->_REQUEST("type_id");
			$img	= $this->_REQUEST("bookimg_spath");
			$overs 	= $this->_REQUEST("overs");
			$audit 	= $this->_REQUEST("audit");
			$intro 	= $this->_REQUEST("intro");
			$click_a= $this->_REQUEST("click_a");
			$click_m= $this->_REQUEST("click_m");
			$click_w= $this->_REQUEST("click_w");
			$recom_a= $this->_REQUEST("recom_a");
			$recom_m= $this->_REQUEST("recom_m");
			$recom_w= $this->_REQUEST("recom_w");
			$sql   = "update fly_book set 
							name='$name',
							writer='$writer',
							typeid='$typeid',
							img='$img',
							overs='$overs',
							audit='$audit',
							intro='$intro',
							click_a='$click_a',
							click_m='$click_m',
							click_w='$click_w',
							recom_a='$recom_a',
							recom_m='$recom_m',
							recom_w='$recom_w'				
			 		where id='$id'";
			if($this->C($this->cacheDir)->update($sql)>=0){
				$this->L("Common")->ajax_json_success("操作成功了吧",'1',"/admin/Book/book_show/id/$id/");
			}		
		}
	}
	
	//查询一条记录
	public function book_get_one($id=""){
		if($id){
			$sql = "select b.*,t.typename from fly_book as b,fly_book_type as t where b.typeid=t.id and  b.id='$id'";
			$one = $this->C($this->cacheDir)->findOne($sql);
			$one["udt"]=date("Y-m-d H:i:s",$one["udt"]);
			return $one;
		}	
	}	
	
	public function book_del(){
		$id	  = $this->_REQUEST("ids");
		//删除小说封面
		$sql  = "delete from fly_book where id in ($id)";
		$this->C($this->cacheDir)->update($sql);
		//删除小说章节
		$sql  = "delete from fly_book_chap where bookid in ($id)";
		$this->C($this->cacheDir)->update($sql);	
		$this->L("Common")->ajax_json_success("操作成功","1","/admin/Book/book_show/");	
	}	
	
	public function book_select(){
		$cusID = $this->_REQUEST("cusID");
		$sql  ="select id,name from fly_book where cusID='$cusID' order by id asc;";
		$list =$this->C($this->cacheDir)->findAll($sql);
		echo json_encode($list);
	}	
		
	public function book_arr($cusID=""){
		$rtArr  =array();
		$where  =empty($cusID)?"":" where cusID='$cusID'";
		$sql	="select id,name from fly_book $where";
		$list	=$this->C($this->cacheDir)->findAll($sql);
		if(is_array($list)){
			foreach($list as $key=>$row){
				$rtArr[$row["id"]]=$row["name"];
			}
		}
		return $rtArr;
	}				
}//
?>