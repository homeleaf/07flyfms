<?php 
/*
 * 采集配置类
 *
 * @copyright   Copyright (C) 2017-2018 07FLY Network Technology Co,LTD (www.07FLY.com) All rights reserved.
 * @license     For licensing, see LICENSE.html or http://www.07fly.top/fms/license
 * @author      kfrs <goodkfrs@QQ.com>
 * @package     admin.CoChap
 * @version     1.0
 * @link       http://www.07fly.top
 */	 
class CoChap extends Action{	
	private $cacheDir='';//缓存目录
	private $co_conf ='';//采集配置
	public function __construct() {
		_instance('Action/sysmanage/Auth');
		$this->co_conf=$this->L('admin/CoConfig')->co_conf();
	}	

	public function co_chap(){
		//**获得传送来的数据作分页处理
		$currentPage= $this->_REQUEST("pageNum");//第几页
		$numPerPage = $this->_REQUEST("numPerPage");//每页多少条
		$currentPage= empty($currentPage)?1:$currentPage;
		$numPerPage = empty($numPerPage)?$GLOBALS["pageSize"]:$numPerPage;
		
		//**************************************************************************
		//**获得传送来的数据做条件来查询
		$searchKeyword	= $this->_REQUEST("searchKeyword");
		$searchValue	= $this->_REQUEST("searchValue");
		
		$where_str = " id>0 ";
		
		if( !empty($searchValue) ){
			$where_str .=" and $searchKeyword like '%$searchValue%'";
		}
		
		
		//**************************************************************************
		$countSql    = "select id from fly_co_chap where $where_str";
		$totalCount  = $this->C($this->cacheDir)->countRecords($countSql);
		$beginRecord = ($currentPage-1)*$numPerPage;
		$sql		 = "select id,name,adt,udt,cotime,status from fly_co_chap
						where $where_str 
						order by cotime desc limit $beginRecord,$numPerPage";
		$list		 = $this->C($this->cacheDir)->findAll($sql);
		foreach($list as $key=>$row){
			$list[$key]['cnt'] = $this->co_chap_url_cnt($row['id']);
			$list[$key]['adt'] = date("Y-m-d H:i:s",$row['adt']);
			$list[$key]['udt'] = date("Y-m-d H:i:s",$row['udt']);
			$list[$key]['cdt'] = date("Y-m-d H:i:s",$row['cotime']);
			$list[$key]['sta'] = ($row['status']==1)?"<b style='color:red;'>暂停</b>":"正常";
		}
		$assignArray = array('list'=>$list,
							 "searchKeyword"=>$searchKeyword,"searchValue"=>$searchValue,
							 "numPerPage"=>$numPerPage,"totalCount"=>$totalCount,"currentPage"=>$currentPage);	
		return $assignArray;
		
	}
	
	public function co_chap_show(){
			$assArr   = $this->co_chap();
			$smarty   = $this->setSmarty();
			$smarty->assign($assArr);
			$smarty->display('admin/co_chap_show.html');	
	}
	
	//测试采集
	public function co_chap_test(){
		$id=$this->_REQUEST("id");
		$sql="select * from fly_co_chap where id='$id'";
		$one=$this->C($this->cacheDir)->findOne($sql);
		$co  = $this->L("FlyCollection");
		$url = $co->co_one_url($one["listurl"],$one["listarea"],$one["listmustrule"],$one["listnorule"]);
		$rule=array(
					"title"=>array($one["title"],$one["title_trim"]),
					"body"=>array($one["body"],$one["body_trim"])
					);
		$body=$co->co_one_page($url[1]['link'],$rule);
		$smarty = $this->setSmarty();
		$smarty->assign(array("one"=>$one,"testlist"=>print_r($url,true),"testbody"=>print_r($body,true)));
		$smarty->display('admin/co_chap_test.html');		
	}
	
	//封面采集增加
	public function co_chap_add(){
		if(empty($_POST)){
			$smarty = $this->setSmarty();
			$smarty->display('admin/co_chap_add.html');	
		}else{
			$rtn=$this->co_chap_add_save();	
			if($rtn>0){
				$this->L("Common")->ajax_json_success("操作成功",'2',"co_chap_add");
			}else{
				$this->L("Common")->ajax_json_error("操作失败");
			}
		}
	}		

	//保存数
	public function co_chap_add_save(){
		$name		= $this->_REQUEST("name");
		$listurl  	= $this->_REQUEST("listurl");
		$ismore		= $this->_REQUEST("ismore");
		$loop_sid  = $this->_REQUEST("loop_sid");
		$loop_eid 	= $this->_REQUEST("loop_eid");
		$loop_add 	= $this->_REQUEST("loop_add");
		$listarea 	= $this->_REQUEST("listarea");
		$listmustrule = $this->_REQUEST("listmustrule");
		$listnorule = $this->_REQUEST("listnorule");
		
		$sql     = "insert into fly_co_chap(name,listurl,ismore,loop_sid,loop_eid,loop_add,
											listarea,listmustrule,listnorule) 
							values('$name','$listurl','$ismore','$loop_sid','$loop_eid','$loop_add',
								  '$listarea','$listmustrule','$listnorule');";										
		if($rtn=$this->C($this->cacheDir)->update($sql)>0){
			return $rtn;
		}else{
			return 0;	
		}

	}

	//针对小说编号增加一条单独的小说章节采集规则
	//parm @bookid  小说编辑
	//   @id 章节模板id号
	public function co_chap_add_book($bookid,$typeid,$bookname,$bookurl,$id){
		
		$sql="select * from fly_co_chap where id='$id'";
		$one=$this->C($this->cacheDir)->findOne($sql);
		$name		= $one["name"]."+".$bookname;
		$listurl  	= $bookurl;//设置小说的采集地址
		$ismore		= $one["ismore"];
		$loop_sid 	= $one["loop_sid"];
		$loop_eid 	= $one["loop_eid"];
		$loop_add 	= $one["loop_add"];
		$listarea 	= $one["listarea"];
		$listmustrule = $one["listmustrule"];
		$listnorule = $one["listnorule"];
		$title 		= $one["title"];
		$title_trim = $one["title_trim"];
		$body		= $one["body"];
		$body_trim	= $one["body_trim"];
		
		$sql = "insert into fly_co_chap(bookid,typeid,name,listurl,ismore,loop_sid,loop_eid,loop_add,
											listarea,listmustrule,listnorule,
											title,title_trim,body,body_trim,adt
											) 
							values('$bookid','$typeid','$name','$listurl','$ismore','$loop_sid','$loop_eid','$loop_add',
								  '$listarea','$listmustrule','$listnorule',
								  '$title','$title_trim','$body','$body_trim','".time()."'
								  )";										
		if($rtn=$this->C($this->cacheDir)->update($sql)>0){
			return $rtn;
		}else{
			return 0;	
		}
	}
	
	
	//修改
	public function co_chap_modify(){
		$id	= $this->_REQUEST("id");
		if(empty($_POST)){
			$sql 		= "select * from fly_co_chap where id='$id'";
			$one 		= $this->C($this->cacheDir)->findOne($sql);	
			$smarty  	= $this->setSmarty();
			$smarty->assign(array("one"=>$one));
			$smarty->display('admin/co_chap_modify.html');	
		}else{//更新保存数据
			$name     = $this->_REQUEST("name");
			$listurl  = $this->_REQUEST("listurl");
			$ismore   = $this->_REQUEST("ismore");
			$loop_sid = $this->_REQUEST("loop_sid");
			$loop_eid = $this->_REQUEST("loop_eid");
			$loop_add = $this->_REQUEST("loop_add");
			$listarea = $this->_REQUEST("listarea");
			$listmustrule = $this->_REQUEST("listmustrule");
			$listnorule = $this->_REQUEST("listnorule");
			
			$bodyurl   	= $this->_REQUEST("bodyurl");
			$title    	= $this->_REQUEST("title");
			$title_trim  = $this->_REQUEST("title_trim");
			$body    	= $this->_REQUEST("body");
			$body_trim  = $this->_REQUEST("body_trim");

			$sql   = "update fly_co_chap set 
							name='$name',
							listurl='$listurl',
							loop_sid='$loop_sid',
							loop_eid='$loop_eid',
							loop_add='$loop_add',
							listarea='$listarea',
							listmustrule='$listmustrule',
							listnorule='$listnorule',
							bodyurl='$bodyurl',
							title='$title',
							title_trim='$title_trim',
							body='$body',
							body_trim='$body_trim'		
			 		where id='$id'";
			if($this->C($this->cacheDir)->update($sql)>=0){
				$this->L("Common")->ajax_json_success("操作成功了吧",'1',"/admin/CoChap/co_chap_show/id/$id/");
			}		
		}
	}
	
	//查询一条记录
	public function co_chap_get_one($id=""){
		if($id){
			$sql 		= "select * from fly_co_chap where id='$id'";
			$one 		= $this->C($this->cacheDir)->findOne($sql);	
			return $one;
		}	
	}	
	
	public function co_chap_del(){
		$id	  = $this->_REQUEST("ids");
		$sql  = "delete from fly_co_chap where id in ($id)";
		$this->C($this->cacheDir)->update($sql);	
		
		$sql  = "delete from fly_co_chap_htmls where co_chap_id in ($id)";
		$this->C($this->cacheDir)->update($sql);
		
		$sql  = "delete from fly_co_chap_urls where co_chap_id in ($id)";
		$this->C($this->cacheDir)->update($sql);
		
		$this->L("Common")->ajax_json_success("操作成功","1","/admin/CoChap/co_chap_show/");	
	}	

	//清除采集规则采集数据
	public function co_chap_remove(){
		$id	  = $this->_REQUEST("id");
		$sql  = "delete from fly_co_chap_htmls where co_book_id in ($id)";
		$this->C($this->cacheDir)->update($sql);	
		$sql  = "delete from fly_co_chap_urls where co_book_id in ($id)";
		$this->C($this->cacheDir)->update($sql);	
		$this->L("Common")->ajax_json_success("操作成功","1","/admin/CoChap/co_chap_show/");	
	}	
	
	
	//得到采集节点的网址数
	public function co_chap_url_cnt($id){
		$sql_0 ="select co_chap_id from fly_co_chap_htmls where co_chap_id='$id';";
		$cnt_0 = $this->C($this->cacheDir)->countRecords($sql_0);
		
		$sql_1 ="select co_chap_id from fly_co_chap_htmls where co_chap_id='$id' and isdown=1;";
		$cnt_1 = $this->C($this->cacheDir)->countRecords($sql_1);
		
		$sql_2 ="select co_chap_id from fly_co_chap_htmls where co_chap_id='$id' and isexport=1;";
		$cnt_2 = $this->C($this->cacheDir)->countRecords($sql_2);
		
		return array('a_cnt'=>$cnt_0,'d_cnt'=>$cnt_1,'i_cnt'=>$cnt_2);
		
	}	

	//采集执行
	public function co_chap_one_coll(){
		//采集项目处理
		$stime	=microtime(true);
		$sureid	=$this->_REQUEST("sureid");
		$co_num	=$this->_REQUEST("co_num");
		if(empty($co_num)) $co_num=1;
		//读取采集规则
		if(empty($sureid)){
			$sql ="select *	from fly_co_chap where bookid>0 and typeid>0 and status=0 order by cotime asc limit 1";
			$one =$this->C($this->cacheDir)->findOne($sql);
			$coid=$one['id'];
		}else{
			$sql ="select * from fly_co_chap where bookid>0 and typeid>0 and id='$sureid' limit 1";
			$one =$this->C($this->cacheDir)->findOne($sql);	
			$coid=$sureid;
		}
		//采集次数处理
		$co	 	=$this->L("FlyCollection");
		$tmsg  ="共采集了【".$co_num."】次, 【本条规则ID：".$one['id']."】【".$one['name']."】<br>";

		//print_r($co_url);
		/* 第二批次 采集内容 写入数据*/
		$rule=array(
					"title"=>array($one["title"],$one["title_trim"]),
					"body"=>array($one["body"],$one["body_trim"])
					);
		
		//第一步下载数
		//下载内容到数据库
		$cfg_co_chap_num=empty($this->co_conf['cfg_co_chap_num'])?"10":$this->co_conf['cfg_co_chap_num'];
		$item_url_sql="select id,name,url from fly_co_chap_htmls where co_chap_id='$coid' and isdown=0  order by id asc  limit 0,$cfg_co_chap_num";
		$item_url_arr=$this->C($this->cacheDir)->findAll($item_url_sql);
		
		foreach($item_url_arr as $item_url_one){
			//采集内容
			$body=$co->co_one_page($item_url_one['url'],$rule);
			$result=array( 'title'=>urlencode($body['title']),'body'=>urlencode($body['body']) );
			//标记采集过的地址
			$result=json_encode($result);
			//$query="update fly_co_chap_htmls set isdown=1,isexport=1,result='$result' where id='".$item_url_one['id']."'";
			$query="update fly_co_chap_htmls set isdown='1',result='$result' where id='".$item_url_one['id']."'";
			$this->C($this->cacheDir)->updt($query);
			//$tmsg .= "章节: 《".$body['title']."》【采集完成】<br>";
		}

		//第二步导出数据
		$book_chap	 =$this->L('admin/BookChap');//采集内容 
		$item_url_sql="select id,name,result from fly_co_chap_htmls where co_chap_id='$coid' and isdown=1 and isexport=0 order by id asc  limit 0,$cfg_co_chap_num";
		$item_url_arr=$this->C($this->cacheDir)->findAll($item_url_sql);
		foreach($item_url_arr as $item_url_one){
			//转换采集的结果内容，成为数组
			$out=json_decode(urldecode($item_url_one['result']),true);
			
			if(empty($out['title']) || empty($out['body']) ) {//判断是否采集到标题和内容，为空时停止采集,
				$stop_co_rule="update fly_co_chap set status='1' where id='$coid'";
				$this->C($this->cacheDir)->updt($stop_co_rule);	
				break;
			}
			//判断入库章节
			$check=$book_chap->book_chap_exitis_name($out['title'],$one['bookid']);
			if($check){
				$tmsg .= "<font color='red'>章节：《".$out['title']."》【存在跳过】</font><br>";
			}else{
				$data	=array("bookid"=>$one['bookid'],"typeid"=>$one['typeid'],"name"=>$out['title'],"content"=>$out['body']);
				$size	=mb_strlen($out['body']);//统计字数
				$chapid	=$book_chap->book_chap_add_save($data);
				$tmsg  .= "章节: 《".$out['title']."》【入库完成】<br>";
			}
			//标记采集过的地址
			//$result=json_encode($out);
			//$query="update fly_co_chap_htmls set isdown=1,isexport=1,result='$result' where id='".$item_url_one['id']."'";
			$query="update fly_co_chap_htmls set isexport=1,result='已导出' where id='".$item_url_one['id']."'";
			$this->C($this->cacheDir)->updt($query);
			
			//设置更新后时间,采集规则
			$update_co_time="update fly_co_chap set udt='".time()."' where id='$coid'";
			$this->C($this->cacheDir)->updt($update_co_time);	
			
			//更新书本的时间
			if(!empty($chapid)){
				$update_book_time="update fly_book set udt='".time()."',lastchapid='$chapid',booksize=booksize+$size where id='".$one['bookid']."'";
				$this->C($this->cacheDir)->updt($update_book_time);	
			}
		}		

		//判断无采集网址之后再去抓取网址----开始
		if(empty($item_url_arr)){
			/*判断是否采集分页*/
			$bookid  =$one['bookid'];
			$typeid  =$one['typeid'];
			$listurl[]=$one['listurl'];

			/* 第一批次，循环采集列表地址 */
			foreach($listurl as $oneurl){
				/*抓取一个地址的超链接*/
				$co_url= $co->co_one_url($oneurl,$one["listarea"],$one["listmustrule"],$one["listnorule"]);
				foreach($co_url as $row){
					/* 第一步判断地址是否已经存在采集列表之中 */
					$url	=$row['link'];
					$md5url	=md5($row['link']);
					$title	=addslashes($row['title']);
					$sql	="select * from fly_co_chap_urls where co_chap_id='$coid' and url='$md5url'";
					$rtn 	=$this->C($this->cacheDir)->findOne($sql);
					/*当不存在时采集地址*/
					if(empty($rtn)){
						$into_htmls_sql="insert into fly_co_chap_htmls(co_chap_id,bookid,typeid,name,url,dtime) values('$coid','$bookid','$typeid','$title','$url','".TIME()."')";
						$this->C($this->cacheDir)->updt($into_htmls_sql);
						$into_urls_sql ="insert into fly_co_chap_urls(co_chap_id,url) values('$coid','$md5url')";
						$this->C($this->cacheDir)->updt($into_urls_sql);
					}
				}//地址循环处理结束
				
			}
			$tmsg .= "抓取新的网址.....";
		}
		//判断无采集网址之后再去抓取网址------结束

		//更新采集后时间时间
		$update_co_time="update fly_co_chap set cotime='".time()."' where id='$coid'";
		$this->C($this->cacheDir)->updt($update_co_time);	
		
		$comm =$this->L('Common');
		$co_num++;	
		//获取程序执行结束的时间 //计算差值
		$etime=microtime(true);
		$dtime=$etime-$stime;  
		
		$tmsg 	  .="<br />当前页面执行时间为：{$dtime} 秒";
		$doneForm  ="<form name='gonext' method='post' action='".ACT."/admin/CoChap/co_chap_one_coll/'>
			<input type='hidden' name='co_num' value='$co_num' />
			<input type='hidden' name='sureid' value='$sureid' />
			</form>\r\n".$comm->gotojs()."\r\n";
		 $comm->put_info($tmsg, $doneForm);

		//$smarty = $this->setSmarty();
		//$smarty->assign(array("one"=>$one,"testlist"=>print_r($url,true),"testbody"=>print_r($body,true)));
		//$smarty->display('admin/co_book_test.html');		
	}
	
						
}//
?>