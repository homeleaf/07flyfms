<?php 
/*
 * 采集配置类
 *
 * @copyright   Copyright (C) 2017-2018 07FLY Network Technology Co,LTD (www.07FLY.com) All rights reserved.
 * @license     For licensing, see LICENSE.html or http://www.07fly.top/fms/license
 * @author      kfrs <goodkfrs@QQ.com>
 * @package     admin.CoBook
 * @version     1.0
 * @link       http://www.07fly.top
 */	 
class CoBook extends Action{	
	private $cacheDir='';//缓存目录
	private $co_conf ='';//采集配置
	public function __construct() {
		_instance('Action/sysmanage/Auth');
		$this->co_conf=$this->L('admin/CoConfig')->co_conf();
	}	
	
	public function co_book($cusID=0){
	
		//**获得传送来的数据作分页处理
		$currentPage = $this->_REQUEST("pageNum");//第几页
		$numPerPage  = $this->_REQUEST("numPerPage");//每页多少条
		$currentPage = empty($currentPage)?1:$currentPage;
		$numPerPage  = empty($numPerPage)?$GLOBALS["pageSize"]:$numPerPage;
		
		//**************************************************************************
		//**获得传送来的数据做条件来查询
		$searchKeyword	   = $this->_REQUEST("searchKeyword");
		$searchValue	   = $this->_REQUEST("searchValue");	
		$where_str = " id>0 ";
		
		if( !empty($searchValue) ){
			$where_str .=" and $searchKeyword like '%$searchValue%'";
		}
	
		//**************************************************************************
		$countSql    = "select * from fly_co_book where $where_str";
		$totalCount  = $this->C($this->cacheDir)->countRecords($countSql);
		$beginRecord = ($currentPage-1)*$numPerPage;
		$sql		 = "select * from fly_co_book
						where $where_str 
						order by cotime desc limit $beginRecord,$numPerPage";	
		$list		 = $this->C($this->cacheDir)->findAll($sql);
		foreach($list as $key=>$row){
			$list[$key]['cnt'] = $this->co_book_url_cnt($row['id']);
			$list[$key]['adt'] = date("Y-m-d H:i:s",$row['adt']);
			$list[$key]['udt'] = date("Y-m-d H:i:s",$row['udt']);
			$list[$key]['cdt'] = date("Y-m-d H:i:s",$row['cotime']);
		}
		$assignArray = array('list'=>$list,
							 "searchKeyword"=>$searchKeyword,"searchValue"=>$searchValue,
							 "numPerPage"=>$numPerPage,"totalCount"=>$totalCount,"currentPage"=>$currentPage);	
		return $assignArray;
		
	}
	
	public function co_book_show(){
		$assArr   = $this->co_book();
		$smarty   = $this->setSmarty();
		$smarty->assign($assArr);
		$smarty->display('admin/co_book_show.html');	
	}
	
	//测试采集
	public function co_book_test(){
		$id=$this->_REQUEST("id");
		$sql="select * from fly_co_book where id='$id'";
		$one=$this->C($this->cacheDir)->findOne($sql);
		
		/*判断是否采集分页*/
		if($one["ismore"]==1){
			$listurl[]=str_replace("(*)",$one["loop_sid"],$one["listurl"]);
		}else{
			$listurl[]=$one["listurl"];
		}
		
		$co  = $this->L("FlyCollection");
		//采集列表
		$url = $co->co_one_url($listurl[0],$one["listarea"],$one["listmustrule"],$one["listnorule"]);
		$rule=array(
					"title"=>array($one["title"],$one["title_trim"]),
					"intro"=>array($one["intro"],$one["intro_trim"]),
					"writer"=>array($one["writer"],$one["writer_trim"]),
					"source"=>array($one["source"],$one["source_trim"]),
					"image"=>array($one["image"],$one["image_trim"]),
					"overs"=>array($one["overs"],$one["overs_trim"]),
					"chap"=>array($one["chap"],$one["chap_trim"])
					);
		//采集内容
		$body=$co->co_one_page($one["bodyurl"],$rule);
		$body['image']=$co->get_images($body['image'],1);
		$smarty = $this->setSmarty();
		$smarty->assign(array("one"=>$one,"testlist"=>print_r($url,true),"testbody"=>print_r($body,true)));
		$smarty->display('admin/co_book_test.html');		
	}


	
	//封面采集增加
	public function co_book_add(){
		if(empty($_POST)){
			$smarty = $this->setSmarty();
			$smarty->display('admin/co_book_add.html');	
		}else{
			$rtn=$this->co_book_add_save();	
			//echo $rtn;
			if($rtn>0){
				$this->L("Common")->ajax_json_success("操作成功",'1',"/admin/CoBook/co_book_add/");
			}else{
				$this->L("Common")->ajax_json_error("操作失败",'1',"/admin/CoBook/co_book_show/");
			}
		}
	}		

	//保存数
	public function co_book_add_save(){
		$name    = $this->_REQUEST("name");
		$listurl  = $this->_REQUEST("listurl");
		$ismore   = $this->_REQUEST("ismore");
		$loop_sid = $this->_REQUEST("loop_sid");
		$loop_eid = $this->_REQUEST("loop_eid");
		$loop_add = $this->_REQUEST("loop_add");
		$listarea = $this->_REQUEST("listarea");
		$listmustrule = $this->_REQUEST("listmustrule");
		$listnorule = $this->_REQUEST("listnorule");
		
		$sql     = "insert into fly_co_book(name,listurl,ismore,loop_sid,loop_eid,loop_add,
											listarea,listmustrule,listnorule) 
							values('$name','$listurl','$ismore','$loop_sid','$loop_eid','$loop_add',
								  '$listarea','$listmustrule','$listnorule');";										
		if($rtn=$this->C($this->cacheDir)->update($sql)>0){
			return $rtn;
		}else{
			return 0;	
		}

	}
	
	//修改
	public function co_book_modify(){
		$id	= $this->_REQUEST("id");
		if(empty($_POST)){
			$sql 		= "select * from fly_co_book where id='$id'";
			$one 		= $this->C($this->cacheDir)->findOne($sql);	
			$smarty  	= $this->setSmarty();
			$smarty->assign(array("one"=>$one));
			$smarty->display('admin/co_book_modify.html');	
		}else{//更新保存数据
			$name    = $this->_REQUEST("name");
			$listurl = $this->_REQUEST("listurl");
			$ismore  = $this->_REQUEST("ismore");
			$loop_sid = $this->_REQUEST("loop_sid");
			$loop_eid = $this->_REQUEST("loop_eid");
			$loop_add = $this->_REQUEST("loop_add");
			$listarea = $this->_REQUEST("listarea");
			$listmustrule = $this->_REQUEST("listmustrule");
			$listnorule = $this->_REQUEST("listnorule");
			
			$bodyurl   	= $this->_REQUEST("bodyurl");
			$title    	= $this->_REQUEST("title");
			$title_trim = $this->_REQUEST("title_trim");
			$intro    	= $this->_REQUEST("intro");
			$intro_trim = $this->_REQUEST("intro_trim");
			$writer    	= $this->_REQUEST("writer");
			$writer_trim = $this->_REQUEST("writer_trim");
			$source    	= $this->_REQUEST("source");
			$source_trim = $this->_REQUEST("source_trim");
			$image    	= $this->_REQUEST("image");
			$image_trim = $this->_REQUEST("image_trim");
			$overs    	= $this->_REQUEST("overs");
			$overs_trim = $this->_REQUEST("overs_trim");
			$chap    	= $this->_REQUEST("chap");
			$chap_trim  = $this->_REQUEST("chap_trim");
			
			$sql   = "update fly_co_book set 
							name='$name',
							listurl='$listurl',
							ismore='$ismore',
							loop_sid='$loop_sid',
							loop_eid='$loop_eid',
							loop_add='$loop_add',
							listarea='$listarea',
							listmustrule='$listmustrule',
							listnorule='$listnorule',
							bodyurl='$bodyurl',
							title='$title',
							title_trim='$title_trim',
							intro='$intro',
							intro_trim='$intro_trim',
							writer='$writer',
							writer_trim='$writer_trim',
							source='$source',
							source_trim='$source_trim',
							image='$image',
							image_trim='$image_trim',
							overs='$overs',
							overs_trim='$overs_trim',
							chap='$chap',
							chap_trim='$chap_trim'							
							
			 		where id='$id'";
			if($this->C($this->cacheDir)->update($sql)>=0){
				$this->L("Common")->ajax_json_success("操作成功了吧",'1',"/admin/CoBook/co_book_show/id/$id/");
			}		
		}
	}
	
	//查询一条记录
	public function co_book_get_one($id=""){
		if($id){
			$sql 		= "select * from fly_co_book where id='$id'";
			$one 		= $this->C($this->cacheDir)->findOne($sql);	
			return $one;
		}	
	}	
	
	//删除采集规则
	public function co_book_del(){
		$id	  = $this->_REQUEST("ids");
		$sql  = "delete from fly_co_book where id in ($id)";
		$this->C($this->cacheDir)->update($sql);
		
		$sql  = "delete from fly_co_book_htmls where co_book_id in ($id)";
		$this->C($this->cacheDir)->update($sql);
		
		$sql  = "delete from fly_co_book_urls where co_book_id in ($id)";
		$this->C($this->cacheDir)->update($sql);
		
		$this->L("Common")->ajax_json_success("操作成功","1","/admin/CoBook/co_book_show/");	
	}	

	//清除采集规则采集数据
	public function co_book_remove(){
		$id	  = $this->_REQUEST("id");
		$sql  = "delete from fly_co_book_htmls where co_book_id in ($id)";
		$this->C($this->cacheDir)->update($sql);	
		$sql  = "delete from fly_co_book_urls where co_book_id in ($id)";
		$this->C($this->cacheDir)->update($sql);	
		
		$this->L("Common")->ajax_json_success("操作成功","1","/admin/CoBook/co_book_show/");	
	}	
	
	//得到采集节点的网址数
	public function co_book_url_cnt($id){
		$sql_0 ="select * from fly_co_book_htmls where co_book_id='$id';";
		$cnt_0 = $this->C($this->cacheDir)->countRecords($sql_0);
		
		$sql_1 ="select * from fly_co_book_htmls where co_book_id='$id' and isdown=1;";
		$cnt_1 = $this->C($this->cacheDir)->countRecords($sql_1);
		
		$sql_2 ="select * from fly_co_book_htmls where co_book_id='$id' and isexport=1;";
		$cnt_2 = $this->C($this->cacheDir)->countRecords($sql_2);
		
		return array('a_cnt'=>$cnt_0,'d_cnt'=>$cnt_1,'i_cnt'=>$cnt_2);
		
	}

	//循环采集执行
	public function co_book_one_coll(){
		//采集项目处理
		$stime	=microtime(true);
		$sureid	=$this->_REQUEST("sureid");
		$co_num	=$this->_REQUEST("co_num");
		if(empty($co_num)) $co_num=1;
		
		//判断是否指定采集ID号，未指定默认按最后采集时间来
		if(empty($sureid)){
			$sql ="select * from fly_co_book order by cotime asc";
			$one =$this->C($this->cacheDir)->findOne($sql);
			$coid=$one['id'];
		}else{
			$sql ="select * from fly_co_book where id='$sureid'";
			$one =$this->C($this->cacheDir)->findOne($sql);	
			$coid=$sureid;
		}
		
		$chap_id=intval($one["chap_id"]);
		$typeid =intval($one["typeid"]);
		
		$co	 	=$this->L("FlyCollection");
		$tmsg  ="共采集了【".$co_num."】次, 【本条规则ID：".$one['id']."】【".$one['name']."】<br>";

		//print_r($co_url);
		/* 第二批次 采集内容 写入数据*/
		$rule=array(
					"title"=>array($one["title"],$one["title_trim"]),
					"intro"=>array($one["intro"],$one["intro_trim"]),
					"writer"=>array($one["writer"],$one["writer_trim"]),
					"source"=>array($one["source"],$one["source_trim"]),
					"image"=>array($one["image"],$one["image_trim"]),
					"overs"=>array($one["overs"],$one["overs_trim"]),
					"chap"=>array($one["chap"],$one["chap_trim"])
					);
		
		$cfg_co_book_num=empty($this->co_conf['cfg_co_chap_num'])?"10":$this->co_conf['cfg_co_book_num'];//单次采集个数
		$item_url_sql="select id,name,url from fly_co_book_htmls where co_book_id='$coid' and isexport=0 order by id asc limit 0,$cfg_co_book_num";
		$item_url_arr=$this->C($this->cacheDir)->findAll($item_url_sql);
		
		$book	=$this->L('admin/Book');//采集内容 
		foreach($item_url_arr as $item_url_one){
			//采集内容
			$body=$co->co_one_page($item_url_one['url'],$rule);
			
			//判断是否采集到标题和章节地址
			if(!empty($body['title']) && !empty($body['chap'])){
				//整理数据
				$data=array(
								"name"=>$body['title'],
								"intro"=>$body['intro'],
								"img"=>$body['image'],
								"writer"=>$body['writer'],
								"overs"=>$body['overs'],
								"typeid"=>$one['typeid'],
							);
				$check=$book->book_exitis_name($body['title']);//判断是否书名存在
				if($check){
					$tmsg .= "<font color='red'>小说：《".$body['title']."》【存在跳过】</font><br>";
				}else{
					if(!empty($data['img'])) $data['img']=$co->get_images($body['image'],1);//下载小说图片
					$bookid	=$book->book_add_save($data);//入库
					$tmsg .= "小说: 《".$body['title']."》【入库完成】<br>";
					//生成小说的采集规则
					$this->L('admin/CoChap')->co_chap_add_book($bookid,$typeid,$body['title'],$body['chap'],$chap_id);
				}
			}
			//标记采集过的地址
			//$result=json_encode($body);
			$query="update fly_co_book_htmls set isdown=1,isexport=1 where id='".$item_url_one['id']."'";
			$this->C($this->cacheDir)->updt($query);
			//设置更新时间,执行一次就更新
			$update_time="update fly_co_book set udt='".time()."' where id='$coid'";
			$this->C($this->cacheDir)->updt($update_time);
			
		}
		
		//判断无采集网址之后再去抓取网址----开始
		if(empty($item_url_arr)){
			$ismore =$one["ismore"];
			$typeid =$one["typeid"];
			$sid	=intval($one["loop_sid"]);
			$eid	=intval($one["loop_eid"]);
			$add	=intval($one["loop_add"]);
			/*判断是否采集分页*/
			if($ismore==1){
				for($i=$sid;$i<$eid;$i=$i+$add){
					$listurl[]=str_replace("(*)",$i,$one["listurl"]);
				}
			}else{
				$listurl[]=$one["listurl"];
			}
			/* 第一批次，循环采集列表地址 */
			foreach($listurl as $oneurl){
				/*抓取一个地址的超链接*/
				$co_url= $co->co_one_url($oneurl,$one["listarea"],$one["listmustrule"],$one["listnorule"]);
				foreach($co_url as $row){
					/* 第一步判断地址是否采集 */
					$url	=$row['link'];
					$md5url	=md5($row['link']);
					$title	=$row['title'];
					$sql	="select id from fly_co_book_urls where co_book_id='$coid' and url='$md5url'";
					$rtn 	=$this->C($this->cacheDir)->findOne($sql);
					/*当不存在时采集地址*/
					if(empty($rtn)){
						$into_htmls_sql="insert into fly_co_book_htmls(co_book_id,typeid,name,url,dtime) values('$coid','$typeid','$title','$url','".TIME()."')";
						$this->C($this->cacheDir)->updt($into_htmls_sql);
						$into_urls_sql ="insert into fly_co_book_urls(co_book_id,url) values('$coid','$md5url')";
						$this->C($this->cacheDir)->updt($into_urls_sql);
					}
				}
			}
			
			$tmsg .= "抓取新的网址.....";
			//更新采集时间,执行一次就更新
			$update_co_time="update fly_co_book set cotime='".time()."' where id='$coid'";
			$this->C($this->cacheDir)->updt($update_co_time);
			
		}
		//判断无采集网址之后再去抓取网址------结束
		
		$comm =$this->L('Common');
		$co_num++;
		//获取程序执行结束的时间 //计算差值
		$etime=microtime(true);
		$dtime=$etime-$stime;  
		
		$tmsg 	  .="<br />当前页面执行时间为：{$dtime} 秒";
		$doneForm 	="<form name='gonext' method='post' action='".ACT."/admin/CoBook/co_book_one_coll/'>
			<input type='hidden' name='co_num' value='$co_num' />
			<input type='hidden' name='sureid' value='$sureid' />
			</form>\r\n".$comm->gotojs()."\r\n";
		 $comm->put_info($tmsg, $doneForm);

		//$smarty = $this->setSmarty();
		//$smarty->assign(array("one"=>$one,"testlist"=>print_r($url,true),"testbody"=>print_r($body,true)));
		//$smarty->display('admin/co_book_test.html');		
	}
	
}//
?>