<?php 
/*
 * 小说章节管理类
 *
 * @copyright   Copyright (C) 2017-2018 07FLY Network Technology Co,LTD (www.07FLY.com) All rights reserved.
 * @license     For licensing, see LICENSE.html or http://www.07fly.top/fms/license
 * @author      kfrs <goodkfrs@QQ.com>
 * @package     admin.BookChap
 * @version     1.0
 * @link       http://www.07fly.top
 */	 
class BookChap extends Action{	
	private $cacheDir='c_admin';//缓存目录
	private $sys_conf='';
	public function __construct() {
		_instance('Action/sysmanage/Auth');
		$this->sys_conf=$this->L('sysmanage/SysConfig')->sys_conf();
	}	

	public function book_chap($cusID=0){
	
		//**获得传送来的数据作分页处理
		$currentPage= $this->_REQUEST("pageNum");//第几页
		$numPerPage = $this->_REQUEST("numPerPage");//每页多少条
		$currentPage= empty($currentPage)?1:$currentPage;
		$numPerPage = empty($numPerPage)?$GLOBALS["pageSize"]:$numPerPage;
		
		//**************************************************************************
		//**获得传送来的数据做条件来查询
		$searchKeyword	= $this->_REQUEST("searchKeyword");
		$searchValue  = $this->_REQUEST("searchValue");
		$bookid	   	  = $this->_REQUEST("bookid");
		
		$where_str = " c.id>0 ";
		
		if( !empty($searchValue) ){
			$where_str .=" and c.$searchKeyword like '%$searchValue%'";
		}
		if( !empty($bookid) ){
			$where_str .=" and c.bookid='$bookid'";
		}
		
		//**************************************************************************
		$countSql	= "select c.id from fly_book_chap as c
					  left join fly_book b on b.id=c.bookid 
					  where $where_str ";
		$totalCount	= $this->C($this->cacheDir)->countRecords($countSql);
		$beginRecord = ($currentPage-1)*$numPerPage;
		$sql		 = "select c.id,c.name,c.adt,c.click,b.name as bookname from fly_book_chap as c
						left join fly_book b on b.id=c.bookid 
						where $where_str
						order by c.id desc limit $beginRecord,$numPerPage";			
		$list		 = $this->C($this->cacheDir)->findAll($sql);
		foreach($list as $key=>$row){
			$list[$key]['adt'] = date("Y-m-d H:i:s",$row['adt']);
		}
		$assignArray = array('list'=>$list,
							 "searchKeyword"=>$searchKeyword,"searchValue"=>$searchValue,
							 "bookid"=>$bookid,
							 "numPerPage"=>$numPerPage,"totalCount"=>$totalCount,"currentPage"=>$currentPage);	
		return $assignArray;
		
	}
	
	public function book_chap_show(){
			$assArr   = $this->book_chap();
			$smarty   = $this->setSmarty();
			$smarty->assign($assArr);
			$smarty->display('admin/book_chap_show.html');	
	}
	
	
	//封面采集增加
	public function book_chap_add(){
		if(empty($_POST)){
			$smarty = $this->setSmarty();
			$smarty->display('admin/book_chap_add.html');	
		}else{
			$bookid=$this->_REQUEST("book_id");
			$typeid=$this->_REQUEST("book_typeid");
			$name  =$this->_REQUEST("name");
			$content=$this->_REQUEST("content");

			$rtn=$this->book_chap_add_save(array('bookid'=>$bookid,'typeid'=>$typeid,'name'=>$name,'content'=>$content));	
			//echo $rtn;
			if($rtn>0){
				$this->L("Common")->ajax_json_success("操作成功",'1',"/admin/BookChap/book_chap_show/bookid//");
			}else{
				$this->L("Common")->ajax_json_error("操作失败");
			}
		}
	}		
	
	//检查章节名是否存在
	public function book_chap_exitis_name($name,$bookid){
		$name = addslashes($name);
		$sql="select name from fly_book_chap where name='$name' and bookid='$bookid';";
		$rtn=$this->C($this->cacheDir)->findOne($sql);
		if(!empty($rtn)) {
			return true;
		}else{
			return false;
		}
	}
	
	//增加小说章节返回增加的ID号
	public function book_chap_add_save($data){
		$name	= addslashes($data["name"]);
		$content= addslashes($data["content"]);
		$bookid = $data["bookid"];
		$typeid = $data["typeid"];
		$nowtime= time();
		
		$file=$this->L("File");
		$storage_mode=$this->sys_conf['cfg_storage'];
		if($storage_mode=='2'){//表示TXT存储
			$content="1";
		}
		$sql ="insert into fly_book_chap(bookid,typeid,name,content,adt) values('$bookid','$typeid','$name','$content','$nowtime');";
		$rtn_chap_id =$this->C($this->cacheDir)->update($sql);
				
		if($rtn_chap_id>0){
			if($storage_mode=='2'){
				$dirname=CACHE."storage".S.$typeid.S.$bookid;
				$file->create_dir($dirname);
				$chaptxt=$dirname.S.$rtn_chap_id.'.txt';
				file_put_contents($chaptxt,$data["content"]);	
			}
			return $rtn_chap_id;
		}else{
			return 0;	
		}
	}
	
	//修改
	public function book_chap_modify(){
		$id	= $this->_REQUEST("id");
		if(empty($_POST)){
			$one 		= $this->book_chap_get_one($id);
			$smarty  	= $this->setSmarty();
			$smarty->assign(array("one"=>$one));
			$smarty->display('admin/book_chap_modify.html');	
		}else{//更新保存数据
			$bookid=$this->_REQUEST("book_id");
			$typeid=$this->_REQUEST("book_typeid");
			$name  =$this->_REQUEST("name");
			$content=$this->_REQUEST("content");
			$sql = "update fly_book_chap set name='$name',content='$content',bookid='$bookid',click='$click',typeid='$typeid' 
					where id='$id'";
			if($this->C($this->cacheDir)->update($sql)>=0){
				$this->L("Common")->ajax_json_success("操作成功了吧",'1',"/admin/BookChap/book_chap_show/id/$id/");
			}		
		}
	}
	
	//查询一条记录
	public function book_chap_get_one($id=""){
		if($id){
			$sql = "select c.*,b.name as bookname from fly_book_chap as c,fly_book as b  where c.id='$id'";
			$one = $this->C($this->cacheDir)->findOne($sql);	
			return $one;
		}	
	}
	
	//查询一本书的最后一条记录
	public function book_chap_last_one($id){
		$sql = "select name from fly_book_chap where id='$id';";
		$one = $this->C($this->cacheDir)->findOne($sql);	
		return $one;
	
	}
	//删除章节
	public function book_chap_del(){
		$id	  = $this->_REQUEST("ids");
		$sql  = "delete from fly_book_chap where id in ($id)";
		$this->C($this->cacheDir)->update($sql);	
		$this->L("Common")->ajax_json_success("操作成功","1","/admin/BookChap/book_chap_show/");	
	}	
	
	

	
	
						
}//
?>