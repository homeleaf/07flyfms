<?php
/**
标签引用标签
param @ typeid 栏目id
param @ bookid 书本id
param @ rows  显示行数
param @ limit 显示行数
param @ titlelen 标题长度
param @ orderby 排序方式

字段标签
bookid	书号
bookname 书名
typename 分类栏目名称
typeid	分类栏目id
adt 入库时间
udt 更新时间
*/

function booklist($params,$content,&$smarty,&$repeat){
	$db=new Database();
	
	extract($params);
	
	if(!isset($assign)) $assign = 'field';
	#注册一个block的索引，照顾smarty的版本
	if(method_exists($smarty,'get_template_vars'))
	{
		$_bindex = $smarty->get_template_vars('_bindex');
	}else{
		$_bindex = $smarty->getVariable('_bindex')->value;
	}
	if(!$_bindex) $_bindex = array();
		
	if($name){
		if(!isset($_bindex[$name]))
		{
			$_bindex[$name] = 1;
		}else{
			$_bindex[$name] ++;	
		}	
	}
	$smarty->assign('_bindex',$_bindex);
	
	#在对象$smarty上注册一个数组以供block使用
	if(!isset($smarty->blocksdata)){
		$smarty->blocksdata = array();	
	}
		
	#获得一个本区块的专属数据存储空间
	$dataindex = md5(__FUNCTION__ . md5(serialize($params)));
	$dataindex = substr($dataindex,0,16);
	#将使用$smarty->blocksdata[$dataindex]来存储
	#填充数据
	if(!$smarty->blocksdata[$dataindex])
	{
		//传入条件
		$where_str = " b.id>0 ";
		if(!empty($typeid)){
			$where_str .=" and b.typeid='$typeid'";
		}elseif(!empty($smarty->typeid)){
			$typeid=$smarty->typeid;
			$where_str .=" and b.typeid='$smarty->typeid'";
		}
		
		//搜索条件
		if(!empty($smarty->searchkey) && !empty($smarty->searchval)){
			$searchkey=$smarty->searchkey;
			$searchval=$smarty->searchval;
			$where_str .=" and b.$searchkey like '%$searchval%'";
		}
		
		if(!empty($smarty->action)){
			if($smarty->action=='overs'){
				$where_str .=" and b.overs='1'";
			}
		}
		
		//传字段参数
		if(!empty($row)){
			$limit_str .=" limit 0,$row ";
		}elseif(!empty($limit)){
			$limit_str .=" limit $limit ";
		}else{
			$limit_str .=" limit 0,20 ";
		}
		
		//按字段排序方式
		if(!empty($orderby)){
			$orderby_str =" b.$orderby ";
		}else{
			$orderby_str =" b.udt ";
		}
		
		//排序方式，升降方式
		if(!empty($orderway)){
			$orderby_str .=" $orderway ";
		}else{
			$orderby_str .=" desc ";
		}

		// 是否分页
		if (isset ( $pagesize )) 
		{
			$countsql="select b.id 
					from fly_book as b left join fly_book_type as t on t.id=b.typeid
					where $where_str";
			$totalNum=$db->countRecords($countsql);
			$currentPage=(!isset($smarty->pagenum))?1:$smarty->pagenum;
			$pagepos=($currentPage-1)*$pagesize;
			$limit_str="limit $pagepos,$pagesize";
			if($smarty->action){
				$pageurl=urlswitch('skurl_page',array($smarty->action,$currentPage));
			}else{
				$pageurl=urlswitch('typeurl_page',array($typeid,$currentPage));
			}			
			$pageurl=preg_replace('#/pagenum/(.*)/#isU','/pagenum/{$page}/',$pageurl);
			$page=new Page(array($totalNum, $pagesize, $currentPage,$pageurl,'')); 
			$pagenav=$page->createPage();
			$smarty->assign('pagelist',$pagenav);
	
		}
		$sql	="select b.id,b.name,b.typeid,b.overs,b.adt,b.udt,b.writer,b.img,b.intro,t.typename 
					from fly_book as b left join fly_book_type as t on t.id=b.typeid
					where $where_str 
					order by $orderby_str $limit_str";
		$list	=$db->findAll($sql);
		foreach($list as $key=>$row){
			$data[$key]=$row;
			//$data[$key]['bookurl']=ACT."/home/Book/book_show/bid/".$row['id']."/";
			//$data[$key]['bookurl']="/book/".$row['id']."/";
			$data[$key]['bookurl']=urlswitch('bookurl',$row['id']);
			$data[$key]['bookimg']=APP."/Cache".$row['img'];
			$data[$key]['overs']=($row['overs']==1)?"已完本":"连载中";
			
		}
		//print_r($data);
		$smarty->blocksdata[$dataindex]=$data;
	}
		#如果没有数据，直接返回null,不必再执行了
	if(!$smarty->blocksdata[$dataindex])
	{
		$repeat = false;
		return '';
	}
	#取一条数据出栈，并把它指派给$assign，重复执行开关置位1
	if(list($key, $item) = each($smarty->blocksdata[$dataindex]))
	{
		$smarty->assign($assign,$item);
		$repeat = true;
	}
	#如果已经到达最后，重置数组指针，重复执行开关置位0
	if(!$item)
	{
		reset($smarty->blocksdata[$dataindex]);
		$repeat = false;
		if($name)
		{
			unset($_bindex[$name]);
			$smarty->assign('_bindex',$_bindex);
		}
	}
	#打印内容
	print $content;
}


?>