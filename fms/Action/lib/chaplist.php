<?php
function chaplist($params,$content,&$smarty,&$repeat){
	$db=_instance('Database','',1);
	extract($params);
	
	if(!isset($assign)) $assign = 'field';
	#注册一个block的索引，照顾smarty的版本
	if(method_exists($smarty,'get_template_vars'))
	{
		$_bindex = $smarty->get_template_vars('_bindex');
	}else{
		$_bindex = $smarty->getVariable('_bindex')->value;
	}
	if(!$_bindex) $_bindex = array();
		
	if($name){
		if(!isset($_bindex[$name]))
		{
			$_bindex[$name] = 1;
		}else{
			$_bindex[$name] ++;	
		}	
	}
	$smarty->assign('_bindex',$_bindex);
	
	#在对象$smarty上注册一个数组以供block使用
	if(!isset($smarty->blocksdata)){
		$smarty->blocksdata = array();	
	}
		
	#获得一个本区块的专属数据存储空间
	$dataindex = md5(__FUNCTION__ . md5(serialize($params)));
	$dataindex = substr($dataindex,0,16);
	#将使用$smarty->blocksdata[$dataindex]来存储
	#填充数据
	if(!$smarty->blocksdata[$dataindex])
	{
		$where_str = " b.id>0 ";
		if(!empty($bookid)){
			$where_str .=" and c.bookid='$bookid'";
		}
		
		if(!empty($smarty->bookid)){
			$bookid	=$smarty->bookid;
			$where_str .=" and c.bookid='$smarty->bookid'";
		}
		
		if(!empty($row)){
			$limit_str .=" limit 0,$row ";
		}elseif(!empty($limit)){
			$limit_str .=" limit $limit ";
		}else{
			$limit_str .=" limit 0,20 ";
		}
		if(!empty($orderby)){
			$orderby_str =" c.$orderby ";
		}else{
			$orderby_str =" c.id ";
		}
		if(!empty($orderway)){
			$orderby_str .=" $orderway ";
		}else{
			$orderby_str .=" desc ";
		}
		// 是否分页
		if (isset ( $pagesize )) 
		{
			$countsql="select c.id,c.name from fly_book_chap as c left join fly_book as b on c.bookid=b.id where $where_str";
			$totalNum=$db->countRecords($countsql);
			$currentPage=(!isset($smarty->pagenum))?1:$smarty->pagenum;
			$pagepos=($currentPage-1)*$pagesize;
			$limit_str="limit $pagepos,$pagesize";
			
			$page=new Page(array($totalNum, $pagesize, $currentPage,  ACT."/home/Book/book_chap/bid/$bookid/pagenum/")); 
			$pagenav=$page->createPage();
			$smarty->assign('pagelist',$pagenav);
	
		}
		$sql	="select c.id,c.name from fly_book_chap as c left join fly_book as b on c.bookid=b.id 
					where $where_str 
					order by $orderby_str $limit_str";
		$list	=$db->findAll($sql);
		foreach($list as $key=>$row){
			$data[$key]=$row;
			//$data[$key]['chapurl']=ACT."/home/Chap/chap_show/cid/".$row['id']."/";
			$data[$key]['chapurl']=urlswitch('chapurl',$row['id']);
		}
		//print_r($data);
		$smarty->blocksdata[$dataindex]=$data;
	}
		#如果没有数据，直接返回null,不必再执行了
	if(!$smarty->blocksdata[$dataindex])
	{
		$repeat = false;
		return '';
	}
	#取一条数据出栈，并把它指派给$assign，重复执行开关置位1
	if(list($key, $item) = each($smarty->blocksdata[$dataindex]))
	{
		$smarty->assign($assign,$item);
		$repeat = true;
	}
	#如果已经到达最后，重置数组指针，重复执行开关置位0
	if(!$item)
	{
		reset($smarty->blocksdata[$dataindex]);
		$repeat = false;
		if($name)
		{
			unset($_bindex[$name]);
			$smarty->assign('_bindex',$_bindex);
		}
	}
	#打印内容
	print $content;
}
?>