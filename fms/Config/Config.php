<?php
/**
 * @CopyRight  (C)2006-2011 07fly Development team Inc.
 * @WebSite    www.07fly.com www.07fly.top
 * @Author     07fly.com <web@07fly.com>
 * @Brief      07flyCRM v1.x
 * @Update     2016.06.11
 * @author:    kfrs
**/
//用户配置
 return array (

	'URLMode'   => 0,			
	'ActionDir' => 'hiddenDir/',
	'htmlExt'  => '.html',
	'ReWrite'  => true,
	'Router'  => array(
 		'typeurl'=>array('rule'=>'\/list\/(\d+)/$','surl'=>'/list/{$1}/','durl'=>'/home/BookType/typelist/tid/{$1}/'),
 		'typeurl_page'=>array('rule'=>'\/list\/(\d+)\/(\d+).html',
							  'surl'=>'/list/{$1}/{$page}.html',
							  'durl'=>'/home/BookType/typelist/tid/{$1}/pagenum/{$2}/'),
	   'skurl'=>array('rule'=>'\/shuku\/(\w+)/$','surl'=>'/shuku/{$1}/','durl'=>'/home/BookType/shuku/action/{$1}'),
 		'skurl_page'=>array('rule'=>'\/shuku\/(\w+)\/(\d+).html',
							  'surl'=>'/shuku/{$1}/{$page}.html',
							  'durl'=>'/home/BookType/shuku/action/{$1}/pagenum/{$2}/'),
 		'bookurl'=>array('rule'=>'\/book\/(\d+)/','surl'=>'/book/{$1}/','durl'=>'/home/Book/book_show/bid/{$1}/'),
 		'downurl'=>array('rule'=>'\/txt\/(\d+)/','surl'=>'/txt/{$1}/','durl'=>'/home/Book/book_down/bid/{$1}/'),
 		'bookchap'=>array('rule'=>'\/chap\/(\d+)/','surl'=>'/chap/{$1}/','durl'=>'/home/Book/book_chap/bid/{$1}/'),
 		'chapurl'=>array('rule'=>'\/chap\/(\d+).html','surl'=>'/chap/{$1}.html','durl'=>'/home/Chap/chap_show/cid/{$1}/'),
 	),
	'Debug'     => true,  
	'Session'   => true,
	'pageSize'  =>20,
	'xml'=>array(
		'path'=>EXTEND.'xml',
		'root'=>'niaomuniao',
	),	
	'DB'=>array(
		'Persistent'=>false,
		'DBtype'    => 'Mysql',
		'DBcharSet' => 'utf8',
		'DBhost'    => 'localhost',
		'DBport'    => '3306',
		'DBuser'    => 'root',
		'DBpsw'     => 'root',
		'DBname'    => 'lqf_book'
	),
	'setSmarty'=>array(
		'template_dir'    => VIEW.'templates',
		'compile_dir'     => _mkdir(CACHE. 'c_templates'),
		'left_delimiter'  => '#{',
		'right_delimiter' => '}#',
	),	
); 
?>